#!/bin/bash
sudo apt-get -y install unison jq python3-virtualenv
if [ ! -d venv ]; then
  virtualenv -p python3 venv
fi

source venv/bin/activate
pip install -r requirements.txt

# generate SSL key and cert files
# openssl req -new -x509 -days 365 -nodes -out server_cert.pem -keyout server_key.pem -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"
