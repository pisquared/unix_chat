#!/usr/bin/env python
import asyncio
import datetime
import json
import os
import sys
import time
from string import ascii_letters, digits

from watchgod import awatch, Change

ROOM_META_FILENAME = ".room"
ROOM_LOCAL_UPDATED_FILENAME = ".local.updated"
HEADER_ROOM_CREATED_TS = "created_ts"
HEADER_ROOM_NAME = "name"
HEADER_MSG_CREATED_TS = "created_ts"
HEADER_MSG_SENDER = "sender"

MSG_FORMAT = "> {sender} ({sent_at})\n{message}\n"
MSG_DATE_FORMAT = '%Y-%m-%d %H:%M:%S'

HELP_USAGE = "\nUsage:\n    chat.py r <room>\n    chat.py w <username> <room>"
HELP_CONNECTED_TO_ROOM = "Connected to room {room}"

with open("config.json", "r") as config_f:
    config = json.load(config_f)
    root_dir = config.get("local_dir")


def valid_username(username):
    return all(c in ascii_letters + digits + '_' for c in username)


def get_milliseconds():
    return int(time.time() * 1000)


def get_or_create_room(room):
    room_dir = os.path.join(root_dir, room)
    if not os.path.exists(room_dir):
        os.makedirs(room_dir)
        with open(os.path.join(room_dir, ROOM_META_FILENAME), 'w') as room_file:
            room_headers = {
                HEADER_ROOM_CREATED_TS: get_milliseconds(),
                HEADER_ROOM_NAME: room,
            }
            room_file.write("\n".join(["{}: {}".format(k, v) for k, v in room_headers.items()]))
    return room_dir


def send_message(message, sender, room):
    now_milliseconds = get_milliseconds()

    room_dir = get_or_create_room(room)

    headers = {
        HEADER_MSG_CREATED_TS: now_milliseconds,
        HEADER_MSG_SENDER: sender,
    }
    message_filename = os.path.join(room_dir, "{}_{}".format(now_milliseconds, sender))
    with open(message_filename, 'w') as message_file:
        message_file.write("\n".join(["{}: {}".format(k, v) for k, v in headers.items()]))
        message_file.write("\n\n")
        message_file.write(message)


def display_message(headers, message):
    sender = headers[HEADER_MSG_SENDER]
    sent_at = datetime.datetime.fromtimestamp(int(headers[HEADER_MSG_CREATED_TS]) / 1000.0).strftime(MSG_DATE_FORMAT)

    print(MSG_FORMAT.format(sender=sender, sent_at=sent_at, message=message))


def read_message(filename):
    with open(filename, 'r') as f:
        is_headers = True
        headers = dict()
        message = ""
        for line in f.readlines():
            if not line.strip():
                is_headers = False
                continue
            if is_headers:
                hk, hv = line.split(":")
                headers[hk.strip()] = hv.strip()
            else:
                message += line
        display_message(headers, message)


def read_messages(room):
    room_dir = get_or_create_room(room)

    last_read = get_last_read(room_dir)
    filenames = list()

    for f in os.listdir(room_dir):
        if f.startswith('.'):
            continue
        ts = f.split('_')[0]
        if ts > last_read:
            filenames.append(f)

    filenames = sorted(filenames)

    for filename in filenames:
        read_message(os.path.join(room_dir, filename))


def get_last_read(room_dir):
    local_fname = os.path.join(room_dir, ROOM_LOCAL_UPDATED_FILENAME)
    if not os.path.exists(local_fname):
        return "0"
    with open(local_fname, 'r') as f:
        return f.readline()


def update_last_read(room_dir):
    local_fname = os.path.join(room_dir, ROOM_LOCAL_UPDATED_FILENAME)
    with open(local_fname, 'w') as f:
        f.write(str(get_milliseconds()))


async def read_loop(room):
    room_dir = get_or_create_room(room)
    print(HELP_CONNECTED_TO_ROOM.format(room=room))
    async for changes in awatch(room_dir):
        for change in changes:
            if change[0] is Change.added:
                if change[1].endswith('.tmp'):
                    continue
                read_message(change[1])
                update_last_read(room_dir)


def prompt_loop(username, room):
    print(HELP_CONNECTED_TO_ROOM.format(room=room))
    while True:
        message = input("> ")
        send_message(message, username, room)


assert len(sys.argv) >= 2, HELP_USAGE

command = sys.argv[1]

if command in ["r", "read"]:
    _room = sys.argv[2]
    assert valid_username(_room), "room not valid"
    read_messages(_room)
    update_last_read(get_or_create_room(_room))
    loop = asyncio.get_event_loop()
    loop.run_until_complete(read_loop(_room))
elif command in ["w", "write"]:
    _username, _room = sys.argv[2:4]
    assert valid_username(_username), "username not valid"
    assert valid_username(_room), "room not valid"
    prompt_loop(_username, _room)
