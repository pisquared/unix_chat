# Unix Chat

This chat uses the Unix philosophies of "Everything is a file" and "Do one thing well". The main program is `chat.py`. It uses a base directory (called `local_dir` in configuration) to create rooms (folders). Inside of the room, the chat messages are in the format `<millisecond>_<user>`. The `local_dir` is (optionally) synced with a remote server.

The files of messages have headers part (key-value pairs separated by `:`, each separated by `\n`) and body part. The headers and body is separated by two `\n` similar to HTTP. 

The `chat.py` can run as *read* or *write*
* As *read* (`chat.py r <room>`) - it reads messages as they from a folder `<local_dir>/<room>`
* As *write* (`chat.py w <username> <room>`) - it writes messages to a folder as a user `<local_dir>/<room>/<millisecond>_<user>`

Synchronisation of messages is a separate process - `sync.py` - this can run as a *client* or *server*. 
* As a *server* (`./sync.py s`) - it listens for connections on `0.0.0.0:<remote_port>`, adds connections as observers and notifies all observers over the established socket connection when a watched directory `remote_dir` changes
* As a *client* (`./sync.py s`) - it connects to a server at `<remote_host>:<remote_port>` to be notified via a socket connection in case `remote_dir` has changed. It also notifies and syncs if its `local_dir` has changed.

## Features
*(As of now - see the TODO below for roadmap)*

* Write messages to a room with a chosen user name
* Read messages by reverse timestamp from a chosen room
* Sync messages to a remote server
* Get notified when remote server has an update and update local file structure
* In theory, it's possible to have two clients communicating without a centralized server at all - the two clients can run the appropriate services, exchange the necessary connection keys etc. But this is not easy at the moment. Actually, nothing is really easy or user friendly at the moment, but it's simple and very extensible.

## Install
Run `INSTALL.sh` (for Ubuntu based distros - take a look at the script or below for inspiration what is required on your distro).

### Project dependencies
* [python >= 3.5](https://www.python.org/) - using `async` methods and eventloops by watchgod
* [watchgod](https://pypi.org/project/watchgod/) - used to watch directories for changes [to be deprecated]
* [unison](https://www.cis.upenn.edu/~bcpierce/unison/) - file synchronizer
* [jq](https://stedolan.github.io/jq/) - read json from bash - used to parse configs from bash scripts [to be deprecated - the script doesn't do so much any more and could be replaced by in-python subprocess call]

## Configure
Edit `chat.config.json` to set local, remote directory and remote connections.

## Run
### Run sync server
The server is expected to be running `./sync.py s` as a service.

### Run sync client
Run `./sync.py c` to sync the local dir to a remote server.

### Run chat client
Run `chat.py r <room>` to read room's messages.

Run `chat.py w <username> <room>` to write to room's messages.

## TODO
### V.1.
[ ] OPTIMISATION: This is a bottleneck resulting in about 4-5 seconds delay in even a minimum setup:
1. Chat writer writes a file
2. Client sync gets notified for new file in dir which leads to `unison` sync
3. Server sync gets notified for new file in dir which leads to syncer socket message
4. Client sync gets socket message and does a `unison` sync
5. Chat reader gets notified for a new file and displays on console

Ideally, `unison` will be used on establishing a connection - to bring folder state to canonical. After that when connected with sync send files directly over that connection to the server which then propagates to all observers. The file needs to have the same `stat` as well. Combine `sync` and `chat` OR Create IPC (socket?) between `chat` and `sync`. 

This will also remove the python `watchgod` dependency since `sync` will connect directly to `chat` in observer-observable way.

[ ] BUG: Double syncing of client - see above - step 4 is unnecessary for same client.

[ ] BUG: `unison` syncs all rooms - sync just the currently connected rooms by receiving an argument

[ ] BUG: Deleting local or remote directories causes `unison` to be way to careful with syncronization

[ ] REFACTOR: Remove `jq` dependency - use python subprocess call and pass arguments from there

#### Auth
[ ] FEATURE: Provide:
* **authentication** - a separate server/service that keeps user-key pairs details and ties in with:
* **authorization** - who is allowed to write to which local/remote directory (e.g. during authentication, local user receives key to a `user@server` with permission to read/write to a particular directory. Could tie with Unix's Access Controls?)

[ ] FEATURE: Sign messages with digital signature, add users public keys for verification, don't sync private keys

#### Interactivity
[ ] FEATURE: Show the latest X messages on read

[ ] FEATURE: Scroll read messages

[ ] FEATURE: Create/join rooms interactively

[ ] FEATURE: Combine views (e.g. via predefined `tmux` config ?)

### V.2.
[ ] FEATURE: Send images or any other kind of files - add a header of content-type and if not text - add location where the file can be found

[ ] FEATURE: Limit size of local dir - delete old messages, tar.gz them or...?

### V.3.
[ ] FEATURE: Web interface


### DONE:
[x] ~Closing a client connection doesn't remove it from `OBSERVERS` list from the server~

[x] ~Killing the server process (closing the server connection) crashes client syncers~

[x] ~BUG: Missing directories are not always handled cleanly~